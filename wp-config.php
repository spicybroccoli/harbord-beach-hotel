<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_hbh');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4E!5[[3gaPkN|F1/.seN:Jcru#,~9tf0, U&?7),peeIZZPVN:#[f[2%;x0+hY?8');
define('SECURE_AUTH_KEY',  '/vx`@+wdgU*qqj2i@q3O`stzn(p!z+cYX Co0Z#qmGKPU5SZinOrUM-(=$FDR> T');
define('LOGGED_IN_KEY',    '(SO)ezHe62|Qd1-Ln/>X-wx[rrq1?}McHg6a$Rde(<%Q.[%IK(C_fdc(CGy-eaP|');
define('NONCE_KEY',        '0;?yCMDsyApwm 7h#CaNf{r&R]>UK>!cbp)1/1JlZ-mw6QjnI(j1HY?:1:#`rIy#');
define('AUTH_SALT',        'X5KP+.4wly)]n!NI#A-PZ!K.1`.P@C<_9Vds|7zU=i**|Ih[1R-4:t^wJk.[]D55');
define('SECURE_AUTH_SALT', 'h-E[t6e6Q:uKD:0/3#&r;,HNld.MS3o,Hn[};0K+^!C_0R8|mA%NCV**%N|a5+n|');
define('LOGGED_IN_SALT',   'F=I.mf*Zofe0|{Y`Ik)<YbF-SacD:c0tS5rD.dYz`n,-*Jyj*AlOo!R=zggr}t?X');
define('NONCE_SALT',       '-[?ho`1p^D-}L0xwt_>Si`xTGrM&THBJ+2-k9w<ZD-e#2^-Bs-mHew)n-*|oaY0V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
